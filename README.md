# Setup the cluster

This has to be done once.    
Open google cloud console and type the commands :

```
cd
mkdir gitrepos
cd gitrepos  
git clone https://gitlab.com/danielbfox/CIPonK8S.git
cd CIPonK8S
./setup.sh 
```

# Deploy the CIP

After having setup the cluster, the CIP can be deployed
Still the git CIPonK8S repo folder, type the command :

```
./deploy.sh
```
The script will display the URL where the different tools are accessible

# Configure gitlab

Go to the gitlab URL and set the root password (usually I use `rootroot`)  
Create a new projet by cloning https://gitlab.com/danielbfox/mynodejscalculatorstateless.git *(Set the repo as Public for easeness)*

# Configure jenkins

Add Kubernetes credential using a service account : 
- Click `Credentials`, then click `System` in the left navigation.
- Click `Global credentials (unrestricted)`.
- Click `Add Credentials` in the left navigation.
- Select `Kubernetes Service Account` from the `Kind` dropdown. This option doesn’t appear unless Jenkins is running inside a Kubernetes Engine or Kubernetes cluster.
- Click the OK button.

Set the number of executors to 0:
- Click `Manage jenkins`, then click `Configure system`
- Enter 0 for the `# of executors`

Add a cloud :
- Still in the `Manage jenkins` page, scroll down to the bottom
- Click `Add new cloud` and select `Kubernetes`
- Fill in the following fieds :
    - `Name` = kubernetes
    - `Kubernetes URL` = https://kubernetes.default.svc.cluster.local
    - `Kubernetes Namespace` = default
    - `Credentials` = Secret text
    - `Jenkins URL` = http://jenkins-ui.default.svc.cluster.local:8080/jenkins
    - `Jenkins tunnel` = jenkins-discovery.default.svc.cluster.local:50000
- Click on `Save`

Create a pipeline `mynodejscalculatorstateless` with :
- `Repository URL` = http://gitlab:8080/gitlab/root/mynodejscalculatorstateless.git
- `Branches specifier` = 1.3 *(or check the branches in the git repo for later versions)*

# Configure nexus 

*(admin credentials are admin/admin123)*

Create a docker-hosted repo with :
- `Name` : docker-hosted
- `Repository connector HTTPS` : 8082

Define it as insecure registry on the nodes, type this command from the git CIPonK8S repo folder

```
./setnexusinsecureregistry.sh
```

# Undeploy the CIP

Still the git CIPonK8S repo folder, type the command :

```
./undeploy.sh
```

# Delete the cluster

Still the git CIPonK8S repo folder, type the command :

```
./cleanup.sh
```
# Todo

- Investigate the push to nexus as a insecure registry (see https://gitlab.com/danielbfox/mynodejscalculatorstateless/blob/1.3/Jenkinsfile)
- Run the built image (see https://gitlab.com/danielbfox/mynodejscalculatorstateless/blob/1.3/Jenkinsfile)
- Build the image with kaniko (see https://gitlab.com/danielbfox/mynodejscalculatorstateless/blob/1.3/Jenkinsfilekaniko)

# Tutorials

Jenkins on Kubernetes : https://cloud.google.com/solutions/jenkins-on-kubernetes-engine  
Configuring the Kubernetes Jenkins plugin : https://cloud.google.com/solutions/configuring-jenkins-kubernetes-engine

# References

Kubernetes Jenkins plugin https://github.com/jenkinsci/kubernetes-plugin/blob/master/README.md

