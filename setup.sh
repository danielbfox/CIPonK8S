#!/bin/bash

ZONE=us-east1-b

echo "Creating the cluster"
echo "--------------------"

gcloud container clusters create cipaas \
	--cluster-version=1.9.7-gke.1 \
	--image-type=COS \
	--machine-type=n1-standard-1 \
	--num-nodes=4 \
	--zone=$ZONE
gcloud container clusters get-credentials cipaas \
	--zone=$ZONE
echo "Cluster nodes (gcloud command) :"
gcloud compute instances list
echo "Cluster nodes (kubectl command) :"
kubectl get nodes

echo "Installing helm"
echo "---------------"

curl -o get_helm.sh https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
chmod +x get_helm.sh
./get_helm.sh

echo "Installing tiller"
echo "-----------------"

kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'      
helm init --service-account tiller --upgrade
helm version

echo "Initializing gitlab docker registry on the different nodes"
echo "----------------------------------------------------------"

# Test is a docker login was already done from the google console
DOCKERCONFIGFILE="$HOME/.docker/config.json"
if [ -f "$DOCKERCONFIGFILE" ]; then
    echo "$DOCKERCONFIGFILE found."
else
    echo "$DOCKERCONFIGFILE not found."
    echo "Please make first a docker login registry.gitlab.com and re-run this script"
    exit 1
fi

# Copy the ~/.docker/config.json to all the cluster nodes
nodes=$(kubectl get nodes -o jsonpath='{range.items[*].metadata}{.name} {end}')
for n in $nodes; do
    gcloud compute ssh $n --zone=$ZONE --command='mkdir -p ~/.docker'
    gcloud compute scp $DOCKERCONFIGFILE $n:$DOCKERCONFIGFILE --zone=$ZONE
done

echo "Installing nginx load balancer"
echo "------------------------------"

# see https://cloud.google.com/community/tutorials/nginx-ingress-gke
helm install --name nginx-ingress stable/nginx-ingress



