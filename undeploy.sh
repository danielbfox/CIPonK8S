#!/bin/bash

ZONE=us-east1-b

echo "Deleting jenkins"
echo "----------------"

kubectl delete deploy jenkins
kubectl delete svc jenkins-ui
kubectl delete svc jenkins-discovery
kubectl delete pvc jenkins-pv-claim
kubectl delete clusterrolebinding jenkins

echo "Deleting gitlab"
echo "----------------"

kubectl delete deploy gitlab
kubectl delete svc gitlab
kubectl delete pvc gitlab-pv-etc-claim
kubectl delete pvc gitlab-pv-varopt-claim
kubectl delete pvc gitlab-pv-varlog-claim

echo "Deleting nexus"
echo "----------------"

kubectl delete deploy nexus
kubectl delete svc nexus
kubectl delete pvc nexus-pv-data-claim
kubectl delete secret nexus

echo "Deleting ingress"
echo "----------------"
kubectl delete ingress cip

echo ""
echo "Deletion complete except the disks :"
gcloud compute disks list --filter="zone:( $ZONE )"

exit 0

