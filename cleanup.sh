#!/bin/bash

ZONE=us-east1-b

echo "Deleting cluster"
echo "================"

gcloud container clusters delete cipaas \
	--zone=$ZONE

exit 0

