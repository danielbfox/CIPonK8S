#!/bin/bash

#read -p 'Please enter your cip name: ' CIPNAME
#if [ -z "$CIPNAME" ]; then
#    echo "ERROR : Please specify a cip name"
#    exit 1
#fi

echo "Deploying jenkins"
echo "-----------------"

#sed -i "s/namespace:/namespace: $CIPNAME/g" jenkins/deployment.yaml
kubectl apply -f jenkins/deployment.yaml
#sed -i "s/namespace:/namespace: $CIPNAME/g" jenkins/service.yaml
kubectl apply -f jenkins/service.yaml
# set the service account used by the jenkins plugin as cluster-admin 
kubectl create clusterrolebinding jenkins \
  --clusterrole=cluster-admin  \
  --serviceaccount=default:default

echo "Getting the nginx ingress controller external IP"
echo "------------------------------------------------"

NGINXIP=$(kubectl get svc nginx-ingress-controller -o json | jq -r ".status.loadBalancer.ingress[0].ip")
for i in {1..10}; do
    echo "try $i/10"
    echo "NGINXIP=$NGINXIP"
    if [ "$NGINXIP" == 'null' ]; then
        sleep 10
        NGINXIP=$(kubectl get svc nginx-ingress-controller -o json | jq -r ".status.loadBalancer.ingress[0].ip")
        continue
    else
        break
    fi
done
if [ "$NGINXIP" == 'null' ]; then
    echo "ERROR : IP address can't be retrieved"
    exit 1
fi

echo ""
echo "Jenkins is now accessible from the URL : http://$NGINXIP/jenkins"
echo "Use the following command to retrieve the initial admin password and unlock jenkins : "
JENKINSPOD=$(kubectl get pod -l app=master -o json | jq -r ".items[].metadata.name")
echo "kubectl exec $JENKINSPOD -- cat /var/jenkins_home/secrets/initialAdminPassword"

echo "Deploying GitLab"
echo "-----------------"

sed -i "s/NGINXIP/$NGINXIP/g" gitlab/deployment.yaml
kubectl apply -f gitlab/deployment.yaml
kubectl apply -f gitlab/service.yaml

echo ""
echo "GitLab is now accessible from the URL : http://$NGINXIP/gitlab"

echo "Deploying Nexus"
echo "---------------"

kubectl apply -f nexus/deployment.yaml
kubectl apply -f nexus/service.yaml
echo -n 'admin' >./nexusu.txt
echo -n 'admin123' >./nexusp.txt
kubectl create secret generic nexus --from-file=./nexusu.txt --from-file=./nexusp.txt

echo ""
echo "Nexus is now accessible from the URL : http://$NGINXIP/nexus"

echo "Creating ingress (using nginx lb)"
echo "---------------------------------"

kubectl apply -f ingress.yaml

exit 0


