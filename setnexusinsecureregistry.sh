#!/bin/bash

ZONE=us-east1-b

echo "Initializing nexus insecure registry on the different nodes"
echo "-----------------------------------------------------------"

NEXUSDOCKERHOSTEDIP=$(kubectl get svc nexus-docker-hosted -o json | jq -r ".spec.clusterIP")
NEXUSDOCKERHOSTEDPORT=$(kubectl get svc nexus-docker-hosted -o json | jq -r ".spec.ports[0].nodePort")

echo "registry is $NEXUSDOCKERHOSTEDIP:$NEXUSDOCKERHOSTEDPORT"

for n in $nodes; do
    gcloud compute ssh $n --zone=$ZONE --command="sudo sed -i 's/-p \/var/--insecure-registry=$NEXUSDOCKERHOSTEDIP:$NEXUSDOCKERHOSTEDPORT -p \/var/g' /etc/default/docker"
    gcloud compute ssh $n --zone=$ZONE --command='sudo systemctl daemon-reload'
    gcloud compute ssh $n --zone=$ZONE --command='sudo systemctl restart docker'
    gcloud compute scp $DOCKERCONFIGFILE $n:$DOCKERCONFIGFILE --zone=$ZONE
done




